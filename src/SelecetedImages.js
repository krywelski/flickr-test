function SelectedImages(name) {
  /**
   * local storage is not available when opening a local file on IE 
   * unless you access it using file://127.0.0.1/c$/pathtofile/file.html
   */
  !localStorage && (l = location, p = l.pathname.replace(/(^..)(:)/, "$1$$"), (l.href = l.protocol + "//127.0.0.1" + p));
  
  this.storage_key = 'SelectedImages_' + name;
}

/**
 * Adding an image to the list of selected images in local storage
 */
SelectedImages.prototype.add = function(image) {
  var list_string = window.localStorage.getItem(this.storage_key) || '';
  var list = list_string.split(',');
  if (list.indexOf(image) == -1) {
    list.push(image);
  }
  window.localStorage.setItem(this.storage_key, list.join(','));
};

/**
 * Removing an image to the list of selected images in local storage
 */
SelectedImages.prototype.remove = function(image) {
  var list_string = window.localStorage.getItem(this.storage_key) || '';
  var list = list_string.split(',');
  var index = list.indexOf(image);
  if (index != -1) {
    list.splice(index, 1);
  }
  window.localStorage.setItem(this.storage_key, list.join(','));
};

/**
 * Checking if the image was previously selected
 */
SelectedImages.prototype.has = function(image) {
  var list_string = window.localStorage.getItem(this.storage_key) || '';
  var list = list_string.split(',');
  var index = list.indexOf(image);
  return index != -1;
};

/**
 * return an array of selected images
 */
SelectedImages.prototype.get = function() {
  var list_string = window.localStorage.getItem(this.storage_key) || '';
  return list_string.split(',');
};
