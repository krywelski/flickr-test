describe("SelectedImages", function() {
  var images;

  beforeEach(function() {
    images = new SelectedImages();
  });

  it("should be able to add an image", function() {
    images.add('image1');
    expect(images.has('image1')).toEqual(true);
  });

  describe("when image has been added", function() {
    
    beforeEach(function() {
      images.add('image2');
    });


    it("should indicate that the image exists", function() {
      expect(images.has('image2')).toEqual(true);
    });

    it("should be able to remove", function() {
      images.remove('image2');
      expect(images.has('image2')).toEqual(false);
    });
  });
});
